import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import Link from "@docusaurus/Link";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import useBaseUrl from "@docusaurus/useBaseUrl";
import styles from "./styles.module.css";
// import Footer from './Footer';

const features = [
  {
    title: "About Me",
    imageUrl: "img/undraw_docusaurus_mountain.svg",
    description: (
      <div className="text-align-center">
        My name is Kelly, and I'm a software developer based in West Coast,
        Canada. I'm a Hack Reactor alumni and a graduate of the Software
        Engineering Immersive in Javascript and Python. Besides progamming,
        baking, gaming, and occasionally touching plants, I love problem solving
        and building new, innovative applications.
      </div>
    ),
  },

  {
    title: "List of Technical Skills",
    imageUrl: "img/undraw_docusaurus_react.svg",
    description: (
      <>
       Programming: Python 3, ES6+, Java, SQL, HTML5, CSS System Design: Monoliths, Microservices, Domain-driven design, Message passing, Agile development cycle Backend: PostgreSQL, Django 4, Docker, RabbitMQ, FastApis Front-End: DOM manipulation, React, Pipelining, Git version control, continuous integration, and continuous deployment.
      </>
    ),
  },
  {
    title: "Education, Experience",
    imageUrl: "img/undraw_docusaurus_tree.svg",
    description: (
      <>
        <ul>
          <li>Computer Science student</li>
          <li>Hack Reactor bootcamp graduate</li> 
          <li>Past Part-time Hostess</li>
          <li>Cook</li>
          <li>Business club Executive in high school</li>
        </ul>
      </>
    ),
  },
];


function Feature({ imageUrl, title, description }) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx("col col--4", styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

export default function Home() {
  const context = useDocusaurusContext();
  const { siteConfig = {} } = context;
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />"
    >
      <header className={clsx("hero hero--primary", styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                "button button--outline button--secondary button--lg",
                styles.getStarted
              )}
              to={useBaseUrl("docs/")}
            >
              Go to projects
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
       
      </main>

    </Layout>
  );
}
