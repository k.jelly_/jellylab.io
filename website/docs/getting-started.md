---
title: Yelp For Movies
slug: /
---

Yoovies is a movie review platform where film connoisseurs and movie-watchers alike can share their opinions. We want a space where people can exchange recomendations and comments to ensure a better experience for everyone.

## Setup Instructions:

Deloyed Version -
https://yoovies.gitlab.io/yelp-for-movies/

Development Version: see below

## Step 1

Clone the repository down to your local machine

```shell
git clone <<repository>>
cd yelp-for-movies
```

## Step 2

CD into the new project directory (yelp-for-movies)

```shell
cd yelp-for-movies
```

## Step 3

Copy/paste the keys (located in the Settings => CI/CD => Variables) into a .env file (in the top directory) using the following format:

```shell
REACT_APP_TMDB_API_KEY=key value
REACT_APP_OMDB_API_KEY=key value
DJWTO_SIGNING_KEY=key value
```

## Step 4

RUN the commands:

```shell
 docker volume create yovies-data

 docker compose build

 docker compose up
```

## Step 5

Open Docker Desktop, all of your containers should be up and running.

Select the 'yelp-for-movies-react-1' container and select the tab icon that opens the developement site in your browser.

## Step 7

Please enable cookies on the website and clear any existing cookies.

### All Done!

Remember to create an account and logging in before creating a review!

![search](../static/img/search-bar.png)
