---
title: Exponential
---

Exponential is a custom search engine built using [Google Search API](https://developers.google.com/custom-search/v1/using_rest) that allows users access to a selective query of information related to their input.

## Setup Instruction

Step 1
Install [Python 3](https://www.python.org/downloads/) and [pip](https://pip.pypa.io/en/stable/installing/) if needed
Install [Node.js and npm](https://nodejs.org/en/download/) if needed


Step 2
Clone repository and change directory to the project root
```
 git clone https://gitlab.com/k-jelly_/exponential-search-engine
```

Step 3
Setup a virtual environment `.venv` (using `python -m virtualenv .venv`).
Activate the `.venv` virtual environment using `source .venv/bin/activate`

Step 4
Run `pip install -r requirements.txt`

Step 5
Cd into directory
`shell cd exponential-search-engine`

Step 6
Run `npm install`

Step 7 
`cd` into base directory and create .env file in the base file path of the project

Step 8
Generating the custom Api Key


- After installing the requirements, go to: `https://developers.google.com/custom-search/v1/overview` and select the `get a key` button
- Select: `Create a new project` from the drop down and hit `next`
- Select `show key` and copy the api key from the browser
- Now in the root project directory, create a file named `.env`
- copy and paste this into the directory, along with your api key: 
 ```shell 
 GOOGLE_API_KEY= ``
 SEARCH_ENGINE_ID=``
 ``` 

Step 9
Create a unique search engine ID
- To generate a unique search engine id, go to `https://programmablesearchengine.google.com/controlpanel/all` and select `Add`
- Name the engine `Exponential` and select, `Entire Web`
- Hit `Create`
- Copy and paste the id into the `.env` file in the `SEARCH_ENGINE_ID` variable created above.
 

## Running the app
- At the project root, make sure you've activated the `.venv` virtual environment
- Run `npm run start:front-end` in one terminal window - this runs [Parcel](https://parceljs.org/) 
- The front-end app should now load on http://127.0.0.1:3000
- In a second terminal window, run `npm run start:back-end` 
- The Django app will load on http://127.0.0.1:8000 

## Nagivation 
- To enter a search, enter words into the input bar at the top. 
- Hit `Search` to get a list of results from query
- Hit the magnifying glass on the left to clear search results 
- Scroll through tab to see all results 
- Returns up to 20 results at a time (due to api restrictions)

Step 5.
Go to `http://localhost:3000/` once server finishes setting up.

You're all set!
