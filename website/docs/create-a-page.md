---
title: CarCar
---

'CarCar' is a multipurpose car service management site that allows businesses to keep track of their appointments, clients, sales, employees, history, inventory, and cars.

## Setup Instruction

Step 1.
Go to https://gitlab.com/k-jelly_/project-beta

Step 2.
Clone repository
`shell git clone https://gitlab.com/k-jelly_/project-beta.git`

Step 3.
Cd into directory
`shell cd project-beta`

Open editor and create a .env file in the base file path of the project with the variables:

```shell
SECRET_KEY_SALES=django-insecure-=0#lozw6m8fg901fvz9(b-$@y*_3)v9tgbo9x2se(ezga0)(mj
SECRET_KEY_SERVICE=django-insecure-6v%&r($8@l1=qo&wl1v34x)x^@uxqxuf#bq9s_uh-q8lmc(zk+
SECRET_KEY_INVENTORY=django-insecure-h_*^s%*58o(szd%qaz!(^ky%nah+=yw+d=mkp_)01(r1@*$2t%
```

- _normally this would be private, but the project cannot run without the secret key. There is no personal information stored._

Step 4.
RUN:

```shell
Docker compose build
Docker compose up
```

Step 5.
Go to `http://localhost:3000/` once server finishes setting up.

You're all set!
