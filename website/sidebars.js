module.exports = {
  projects: [
    {
      type: "category",
      label: "Projects",
      items: [
        "getting-started",
        "create-a-page",
        "thank-you"
      ],
    },
  ],
};
