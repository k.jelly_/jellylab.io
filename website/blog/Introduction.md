---
slug: Introduction
title: ""
---

## Introduction

Hi! My name is Kelly, and I'm a software developer based in West Coast, Canada. I hope you enjoyed browsing through my website. I'd love to get to know you so feel free to reach out on LinkedIn or via email if you have any questions/comments.

### Background

I'm a recent graduate of the Hack Reactor Immersive in Javascript and Python, and a part time student at the University of British Columbia. I'm working towards a degree in computer science and have about three years of experience in total and one year worth of experience in software developing. Besides progamming, baking, gaming, and occasionally touching plants, I love learning new innovative ways to solve exisiting problems and spending time with my family. I hope you enjoyed the site and found it relatively easy to navigate.

I'm an optimistic, value-driven individual who prioritizes my team and feedback I get for improvement. I take pride in my communication skills, patience, and positive mindset.

Will be adding more in the weeks to come!

### Technical Skills

Programming: Python 3, ES6+, Java, SQL, HTML5, CSS
System Design: Monoliths, Microservices, Domain-driven design, Message passing, Agile development cycle
Backend: PostgreSQL, Django 4, Docker, RabbitMQ, FastApis
Front-End: DOM manipulation, React, Pipelining, Git version control, continuous integration, and continuous deployment.

### Education

- Hack Reactor Certificate in Software Engineering with JavaScript and Python
- B. Sc. + specialization in Computer Science (in progress)

### Previous Work Experience

- Hostess (2022)
- Cook (2019)

## Achievements 
- Fourth place in the lightweight womans 4+ at the Canadian Secondary School Rowing Championships 
- Recipient of BC Achievement Scholarship for graduates with the top academic achievement in the province. 
