
/** @type {import('@docusaurus/types').DocusaurusConfig, import React from "react", import { BsFacebook, BsInstagram, BsLinkedin } from "react-icons/bs",import { SiGitlab } from "react-icons/si" } */
module.exports = {
  title: `Kelly's Personal Portfolio`,
  tagline: `Welcome to my website! \r\n Please reach out on LinkedIn or via email if you would like to connect. `,
  url: "https://jelly.gitlab.io",
  baseUrl: "/jelly.gitlab.io/",
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/favicon.ico",
  organizationName: "k-jelly_", // Usually your GitHub org/user name.
  projectName: "Personal Projects", // Usually your repo name.
  themeConfig: {
    navbar: {
      title: "",
      logo: {
        alt: "My Site Logo",
        src: "img/logo.svg",
      },
      items: [
        {
          to: "docs/",
          activeBasePath: "docs",
          label: "Projects",
          position: "left",
        },
        { to: "blog/", label: "About me", position: "left" },
        {
          href: "https://github.com/k-jelly1",
          label: "Git Repository",
          position: "right",
        },
      ],
    },
    footer: {
      style: "dark",
      links: [
        {
          title: "Projects",
          items: [
            {

              label: "Yelp for Movies",
              to: "docs/",
              
            },
            {
              label: "CarCar",
              to: "docs/create-a-page/",
            },
            {
              label: "Exponential",
              to: "docs/thank-you/",
            },

          ],
        },
        
        {
          title: "Contact",
          items: [
            {
              tagName: 'link',
              label: "LinkedIn",
              href: "https://www.linkedin.com/in/jiang-kelly/"
            
            
            },
            {
            
              // img_url: "https://cdn.dribbble.com/users/4598/screenshots/4901318/media/4e75b08fcf6a486b900636e2d707e4d6.jpg?compress=1&resize=400x300",
              label: "GitLab",
              href: "https://gitlab.com/k-jelly_",

            },
            // {
            //   label: "GitHub",
            //   href: "https://github.com/KERTLES"
            // }
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} My Project, Inc. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      "@docusaurus/preset-classic",
      {
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
          // Please change this to your repo.
          editUrl: "https://gitlab.com/k-jelly_/jelly.gitlab.io.git",
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            "https://github.com/facebook/docusaurus/edit/master/website/blog/",
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      },
    ],
  ],
};
